#!/usr/bin/env bash

# Made by Fernando "maroto"
# Run anything in the filesystem right before being "mksquashed"
# ISO-NEXT specific cleanup removals and additions (08-2021) @killajoe and @manuel
# Forked for VNux by th1nhhdk

script_path=$(readlink -f ${0%/*})
work_dir=work

# Adapted from AIS. An excellent bit of code!
arch_chroot(){
    arch-chroot $script_path/${work_dir}/x86_64/airootfs /bin/bash -c "${1}"
}

do_merge(){

arch_chroot "

echo '-> Configuring livesession settings and user...'
# prepare livesession settings and user
sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
sed -i 's/#\(vi_VN\ UTF-8\)/\1/' /etc/locale.gen
locale-gen
ln -sf /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime
chmod 700 /root
useradd -m -c 'VNux' -p $(perl -e'print crypt("vnux", "th1nhhdk")') -G 'wheel' -s /bin/zsh vnux
sed -i 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers

# remove bloat
echo '-> Removing bloat...'
pacman -Rcs --noconfirm discover

# enable systemd services
echo '-> Configuring systemd services...'
ln -s /usr/lib/systemd/system/sddm-plymouth.service /etc/systemd/system/display-manager.service
systemctl enable gpm.service
systemctl disable systemd-networkd.service
systemctl enable NetworkManager.service
systemctl enable bluetooth.service
systemctl enable vboxservice.service

# Fix PGP
echo '-> Fixing pacman PGP...'
pacman-key --init
pacman-key --populate archlinux VNux
pacman-key --recv-key D6C9020EE08C8E4F --keyserver keyserver.ubuntu.com
pacman-key --lsign-key D6C9020EE08C8E4F
pacman-key --recv-key 3056513887B78AEB --keyserver keyserver.ubuntu.com
pacman-key --lsign-key 3056513887B78AEB
pacman-key --populate chaotic

# Update mirrors
echo '-> Updating mirrors...'
reflector -c VN,CN --sort rate --save /etc/pacman.d/mirrorlist

"

}

#################################
########## STARTS HERE ##########
#################################

do_merge
