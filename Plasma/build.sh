#!/bin/bash

set -e -u

if [ -d "./work" ]; then
	echo '-> Deleting work folders...'
	sudo ./resetrepo.sh
fi

echo '-> Deleting pacman cache...'
sudo pacman -Sc --noconfirm

echo '-> Building iso...'
sudo ./mkarchiso -v ~/git/VNux/VNux-iso/Plasma

echo '-> chowning iso...'
sudo chown $(whoami) {out,out/*}
