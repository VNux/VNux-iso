#!/usr/bin/env bash
# shellcheck disable=SC2034

iso_name="VNux-Plasma"
iso_label="VNux_$(date +%Y%m)"
iso_publisher="VNux Team <https://vnuxlinux.pages.dev>"
iso_application="VNux GNU/Linux Plasma Live/Rescue CD"
iso_version="$(date +%Y.%m)a"
install_dir="VNux"
buildmodes=('iso')
bootmodes=('bios.syslinux.mbr' 'bios.syslinux.eltorito' 'uefi-x64.systemd-boot.esp' 'uefi-x64.systemd-boot.eltorito')
arch="x86_64"
pacman_conf="pacman.conf"
airootfs_image_type="squashfs"
airootfs_image_tool_options=('-comp' 'xz' '-Xbcj' 'x86' '-b' '1M' '-Xdict-size' '1M')
file_permissions=(
  ["/etc/shadow"]="0:0:400"
  ["/root"]="0:0:750"
  ["/root/.automated_script.sh"]="0:0:755"
)
