#!/bin/bash

set -e -u

if [ -d "./work" ]; then
	echo '-> Deleting work folders...'
	sudo ./resetrepo.sh
fi

echo '-> Deleting pacman cache...'
sudo pacman -Sc --noconfirm

echo '-> Building iso...'
sudo ./mkVNuxiso -v $(pwd)

echo '-> Chowning iso...'
sudo chown $(whoami) {out,out/*}

echo '-> Generating checksum...'
sha512sum out/*.iso

if [ $(whoami) = "th1nhhdk" ]; then
	echo '-> Signing iso...'
	gpg -b out/*.iso
fi